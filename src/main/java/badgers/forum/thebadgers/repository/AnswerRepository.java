package badgers.forum.thebadgers.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import badgers.forum.thebadgers.model.Answer;

public interface AnswerRepository extends JpaRepository<Answer, Long> {
}
