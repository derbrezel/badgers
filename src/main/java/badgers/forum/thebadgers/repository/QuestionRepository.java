package badgers.forum.thebadgers.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import badgers.forum.thebadgers.model.Question;

public interface QuestionRepository extends JpaRepository<Question, Long> {
}
