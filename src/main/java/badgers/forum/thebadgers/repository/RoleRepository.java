package badgers.forum.thebadgers.repository;

import badgers.forum.thebadgers.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Long> {
    public Role findByRole(String role);
}
