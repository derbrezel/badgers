package badgers.forum.thebadgers.service;


import badgers.forum.thebadgers.model.Answer;
import badgers.forum.thebadgers.model.Question;
import badgers.forum.thebadgers.repository.AnswerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor

@Service

public class AnswerService {
    private final AnswerRepository answerRepository;
    private final QuestionService questionService;

    public List<Answer> findAll(){
        return answerRepository.findAll();
    }

    public List<Answer> findAllSorted(){
        return answerRepository.findAll(Sort.by(Sort.Direction.ASC, "createdAt"));
    }

    public Answer save(Answer answer){
        return answerRepository.save(answer);
    }

    public void addAnswerToQuestion (long id, Answer answer){
        questionService.addAnswerToQuestion(id, answer);
    }

    public boolean isAnswerToQuestion(long answerId){
        Optional<Answer> answer = answerRepository.findById(answerId);
        return answer.map(Answer::isAnswerToQuestion).orElse(false);
    }

    public void setAnswerAsAnswerForQuestion(long questionId, long answerId){
        Optional<Question> question = questionService.findQuestionById(questionId);
        Optional<Answer> answer = answerRepository.findById(answerId);

        if(question.isPresent() && answer.isPresent()){
            if(!question.get().isAnswered() && !answer.get().isAnswerToQuestion()){
                answer.get().setAnswerToQuestion(true);
                answerRepository.save(answer.get());
                questionService.setQuestionAswered(questionId);
            }
        }
    }


}
