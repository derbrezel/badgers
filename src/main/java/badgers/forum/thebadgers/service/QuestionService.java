package badgers.forum.thebadgers.service;


import badgers.forum.thebadgers.model.Answer;
import badgers.forum.thebadgers.model.Question;
import badgers.forum.thebadgers.repository.QuestionRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.*;

@RequiredArgsConstructor

@Service

public class QuestionService {
    private final QuestionRepository questionRepository;

    public List<Question> findAll() {
        return questionRepository.findAll();
    }

    public Optional<Question> findQuestionById(long id) {
        return questionRepository.findById(id);
    }

    public Question save(Question question) {
        return questionRepository.save(question);
    }

    public List<Answer> findAnswersForQuestion(long id) {
        Optional<Question> question = questionRepository.findById(id);

        List<Answer> returnList = new ArrayList<>();
        if (question.isPresent()) {
            Set<Answer> answers = question.get().getAnswers();
            returnList.addAll(answers);
        }
        returnList.sort(Comparator.comparing(Answer::getCreatedAt));
        return returnList;
    }

    public void addAnswerToQuestion(long id, Answer answer) {
        Optional<Question> question = questionRepository.findById(id);
        if (question.isPresent()) {
            if(!question.get().isAnswered()) {
                Set<Answer> answers = question.get().getAnswers();
                if (answers == null) {
                    answers = new HashSet<>();
                }
                answers.add(answer);
                question.get().setAnswers(answers);
                questionRepository.save(question.get());
            }
        }
    }

    public List<Question> findUnanswerdQuestions(){
        List<Question> questions = questionRepository.findAll();
        List<Question> returnList = new ArrayList<>();
        questions.forEach(question -> {
            if(question.getAnswers()== null || question.getAnswers().size() == 0){
                returnList.add(question);
            }
        });
        return returnList;
    }

    public void setQuestionAswered(long id){
        Optional<Question> question = findQuestionById(id);
        if(question.isPresent()) {
            question.get().setAnswered(true);
            questionRepository.save(question.get());
        }
    }

    public boolean isQuestionAnswered(long id){
        Optional<Question> question = findQuestionById(id);
        return question.map(Question::isAnswered).orElse(false);
    }
}
