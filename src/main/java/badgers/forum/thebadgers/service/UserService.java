package badgers.forum.thebadgers.service;

import badgers.forum.thebadgers.model.User;

import java.util.List;


public interface UserService {

    public User findByUsername(String username) ;
    public User saveUser(User user);
    public List<User> findAll();
}
