package badgers.forum.thebadgers.model;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data

@Entity
@Table(name = "answer")
public class Answer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private long userId;
    private String content;

    @PrePersist
    protected void onCreate(){
        createdAt = new Date();
    }
    private Date createdAt;
    private boolean isAnswerToQuestion;
}
