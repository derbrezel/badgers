package badgers.forum.thebadgers.model;

import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Data

@Entity
@Table(name = "question")
public class Question {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private long userId;
    private String title;
    private String content;
    @CreationTimestamp
    private Date createdAt;
    @OneToMany
    private Set<Answer> answers;
    private boolean isAnswered;
}
