package badgers.forum.thebadgers.controllers;


import badgers.forum.thebadgers.repository.RoleRepository;
import badgers.forum.thebadgers.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Principal;

@Slf4j
@RequiredArgsConstructor

@Controller
public class IndexController {
    private final UserService userService;

    @Autowired
    private RoleRepository roleRepository;


    @RequestMapping("/")
    String index(Principal principal) {
        return principal != null ? "index" : "index-guest";
    }

    @RequestMapping("/createQuestion")
    String createQuestion() {
        return "createQuestion";
    }

}
