package badgers.forum.thebadgers.controllers;

import badgers.forum.thebadgers.mapper.AnswerMapper;
import badgers.forum.thebadgers.mapper.QuestionMapper;
import badgers.forum.thebadgers.model.Answer;
import badgers.forum.thebadgers.model.Question;
import badgers.forum.thebadgers.service.AnswerService;
import badgers.forum.thebadgers.service.QuestionService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Date;

@Slf4j
@RequiredArgsConstructor

@Controller
public class TestDataController {
    private final QuestionService questionService;
    private final QuestionMapper questionMapper;
    private final AnswerService answerService;
    private final AnswerMapper answerMapper;


    @GetMapping("/createTestData")
    String createTestData() {
        for (int i = 0; i < 20; i++) {
            Question question = new Question();
            question.setCreatedAt(new Date());
            question.setAnswered(false);
            question.setTitle(i + "title");
            question.setContent(i + "here are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour");
            questionService.save(question);
            if(i%2 == 0) {
                for (int j = 0; j < 5; j++) {
                    Answer answer = new Answer();
                    answerService.save(answer);
                    answer.setContent(i + "content");
                    answerService.save(answer);
                    questionService.addAnswerToQuestion(question.getId(), answer);
                }
            }
        }
        //blablabla
        return "redirect:/";
    }
}
