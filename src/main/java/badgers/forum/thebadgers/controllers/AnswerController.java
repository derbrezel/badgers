package badgers.forum.thebadgers.controllers;

import badgers.forum.thebadgers.DTO.AnswerDTO;
import badgers.forum.thebadgers.mapper.AnswerMapper;
import badgers.forum.thebadgers.model.Answer;
import badgers.forum.thebadgers.service.AnswerService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RequiredArgsConstructor

@Controller
public class AnswerController {
    private final AnswerService answerService;
    private final AnswerMapper answerMapper;

   @GetMapping("/testAnswers")
    public ResponseEntity<List<AnswerDTO>> findAll() {
        return ResponseEntity.ok(answerMapper.toAnswerDTOs(answerService.findAllSorted()));
    }

    @RequestMapping(value = "/createAnswer/{currentQuestionId}", method = RequestMethod.POST, consumes = MediaType.ALL_VALUE)
    String addAnswer(@RequestParam String newAnswer, @PathVariable long currentQuestionId){
        Answer answer = new Answer();
        answer.setContent(newAnswer);
        answerService.save(answer);
        answerService.addAnswerToQuestion(currentQuestionId, answer);
        return "redirect:../post/" + currentQuestionId;
    }

    @RequestMapping(value = "/setAnswerAsAnswerForQuestion/{answerId}/{currentQuestionId}", method = RequestMethod.POST, consumes = MediaType.ALL_VALUE)
    String AnswerAsAnswerForQuestion(@PathVariable long answerId, @PathVariable long currentQuestionId){
        answerService.setAnswerAsAnswerForQuestion(answerId,currentQuestionId);
        return "redirect:../../post/" + currentQuestionId;
    }
}
