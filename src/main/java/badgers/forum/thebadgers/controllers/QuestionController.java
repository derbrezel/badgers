package badgers.forum.thebadgers.controllers;

import badgers.forum.thebadgers.DTO.QuestionDTO;
import badgers.forum.thebadgers.mapper.QuestionMapper;
import badgers.forum.thebadgers.service.QuestionService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RequiredArgsConstructor

@Controller
public class QuestionController {
    private final QuestionService questionService;
    private final QuestionMapper questionMapper;

    @GetMapping("/testQuestions")
    public ResponseEntity<List<QuestionDTO>> findAll() {
        return ResponseEntity.ok(questionMapper.toQuestionDTOs(questionService.findAll()));
    }

    @RequestMapping(value = "/createQuestion", method = RequestMethod.POST, consumes = MediaType.ALL_VALUE)
    String addQuestion(@RequestParam String inputTitle,@RequestParam String inputDescription){
        QuestionDTO questionDTO = new QuestionDTO();
        questionDTO.setContent(inputDescription);
        questionDTO.setTitle(inputTitle);
        questionDTO.setAnswered(false);
        questionService.save(questionMapper.toQuestin(questionDTO));
        //blablabla
        return "index";
    }

    @GetMapping("/")
    public String getAllQuestions(Model model) {
        model.addAttribute("questions", questionService.findAll());

        return "index-guest";
    }

    @GetMapping("/getUnansweredQuestions")
    public String getUnansweredQuestions(Model model) {
        model.addAttribute("allunansweredQuestions", questionService.findUnanswerdQuestions());
        return "unansweredQuestions";
    }

    @RequestMapping(value = "/post/{postId}", method = RequestMethod.GET)
    public String getPost(Model model, @PathVariable Long postId) {
        if(postId == null){
            return "redirect:/";
        }
        model.addAttribute("currentQuestion", questionService.findQuestionById(postId));
        model.addAttribute("answers", questionService.findAnswersForQuestion(postId));
        return "post";
    }
}
