package badgers.forum.thebadgers.mapper;

import badgers.forum.thebadgers.DTO.QuestionDTO;
import badgers.forum.thebadgers.model.Question;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface QuestionMapper {
    QuestionDTO toQuestionDTO(Question question);

    List<QuestionDTO> toQuestionDTOs(List<Question> questions);

    Question toQuestin(QuestionDTO questionDTO);
}
