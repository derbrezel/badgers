package badgers.forum.thebadgers.mapper;

import badgers.forum.thebadgers.DTO.AnswerDTO;
import badgers.forum.thebadgers.model.Answer;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper
public interface AnswerMapper {
    AnswerDTO toAnswerDTO(Answer answer);

    List<AnswerDTO> toAnswerDTOs(List<Answer> answers);

    Answer toAnswer(AnswerDTO answerDTO);
}
