package badgers.forum.thebadgers.DTO;


import lombok.Data;

import java.util.Date;

@Data

public class AnswerDTO {
    private long id;
    private long userId;
    private String content;
    private Date createdAt;
    private boolean isAnswerToQuestion;
}
