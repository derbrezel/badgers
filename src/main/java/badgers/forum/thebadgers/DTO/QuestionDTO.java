package badgers.forum.thebadgers.DTO;

import badgers.forum.thebadgers.model.Answer;
import lombok.Data;

import java.util.Date;
import java.util.Set;

@Data

public class QuestionDTO {

    private long id;
    private long userId;
    private String title;
    private String content;
    private Date createdAt;
    private Set<Answer> answers;
    private boolean isAnswered;
}
