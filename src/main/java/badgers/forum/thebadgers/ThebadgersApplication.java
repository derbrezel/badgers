package badgers.forum.thebadgers;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ThebadgersApplication  {

    public static void main(String[] args) {
        SpringApplication.run(ThebadgersApplication.class, args);
    }

}
